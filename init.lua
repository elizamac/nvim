require("eliza.config")
require("eliza.lazy")
-- [[ Basic Autocommands ]]
--  See `:help lua-guide-autocommands`

-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd("TextYankPost", {
	desc = "Highlight when yanking (copying) text",
	group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
	callback = function()
		vim.highlight.on_yank()
	end,
})
-- NOTE: Encrypt files with sops. Doesn't seem to work on Windows
-- {
-- 	"lucidph3nx/nvim-sops",
-- 	event = { "BufEnter" },
-- 	opts = {
-- 		-- Custom options go here
-- 		-- Default options are:
-- 		-- enabled = true,
-- 		-- debug = false,
-- 		-- binPath = 'sops', -- assumes its on $PATH
-- 		-- defaults = { -- overriding any env vars as needed
-- 		--   awsProfile = 'AWS_PROFILE',
-- 		--   ageKeyFile = 'SOPS_AGE_KEY_FILE',
-- 		--   gcpCredentialsPath = 'GOOGLE_APPLICATION_CREDENTIALS',
-- 		-- },
-- 		debug = true,
-- 	},
-- },
-- NOTE: Plugins can also be configured to run Lua code when they are loaded.
--
-- This is often very useful to both group configuration, as well as handle
-- lazy loading plugins that don't need to be loaded immediately at startup.
--
-- For example, in the following configuration, we use:
--  event = 'VimEnter'
--
-- which loads which-key before all the UI elements are loaded. Events can be
-- normal autocommands events (`:help autocmd-events`).
--
-- Then, because we use the `config` key, the configuration only runs
-- after the plugin has been loaded:
--  config = function() ... end

--  { -- Useful plugin to show you pending keybinds.
--    'folke/which-key.nvim',
--    event = 'VimEnter', -- Sets the loading event to 'VimEnter'
--    config = function() -- This is the function that runs, AFTER loading
--      require('which-key').setup()
--
--      -- Document existing key chains
--      require('which-key').register {
--        ['<leader>c'] = { name = '[C]ode', _ = 'which_key_ignore' },
--        ['<leader>d'] = { name = '[D]ocument', _ = 'which_key_ignore' },
--        ['<leader>r'] = { name = '[R]ename', _ = 'which_key_ignore' },
--        ['<leader>s'] = { name = '[S]earch', _ = 'which_key_ignore' },
--        ['<leader>w'] = { name = '[W]orkspace', _ = 'which_key_ignore' },
--        ['<leader>t'] = { name = '[T]oggle', _ = 'which_key_ignore' },
--        ['<leader>h'] = { name = 'Git [H]unk', _ = 'which_key_ignore' },
--      }
--      -- visual mode
--      require('which-key').register({
--        ['<leader>h'] = { 'Git [H]unk' },
--      }, { mode = 'v' })
--    end,
--  },

-- NOTE: The import below can automatically add your own plugins, configuration, etc from `lua/custom/plugins/*.lua`
--    This is the easiest way to modularize your config.
--
--  Uncomment the following line and add your plugins to `lua/custom/plugins/*.lua` to get going.
--    For additional information, see `:help lazy.nvim-lazy.nvim-structuring-your-plugins`
-- { import = 'custom.plugins' },
--}

-- Smart Splits keybindings
-- resizing splits
-- these keymaps will also accept a range,
-- for example `10<A-h>` will `resize_left` by `(10 * config.default_amount)`
vim.keymap.set("n", "<A-h>", require("smart-splits").resize_left)
vim.keymap.set("n", "<A-j>", require("smart-splits").resize_down)
vim.keymap.set("n", "<A-k>", require("smart-splits").resize_up)
vim.keymap.set("n", "<A-l>", require("smart-splits").resize_right)
-- moving between splits
vim.keymap.set("n", "<C-h>", require("smart-splits").move_cursor_left)
vim.keymap.set("n", "<C-j>", require("smart-splits").move_cursor_down)
vim.keymap.set("n", "<C-k>", require("smart-splits").move_cursor_up)
vim.keymap.set("n", "<C-l>", require("smart-splits").move_cursor_right)
vim.keymap.set("n", "<C-\\>", require("smart-splits").move_cursor_previous)
-- swapping buffers between windows
vim.keymap.set("n", "<leader><leader>h", require("smart-splits").swap_buf_left)
vim.keymap.set("n", "<leader><leader>j", require("smart-splits").swap_buf_down)
vim.keymap.set("n", "<leader><leader>k", require("smart-splits").swap_buf_up)
vim.keymap.set("n", "<leader><leader>l", require("smart-splits").swap_buf_right)

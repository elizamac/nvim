-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are loaded (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Map open explorer DISABLED
-- vim.keymap.set("n", "<leader>ep", vim.cmd.Ex)

-- [[ Basic Keymaps ]]
--  See `:help vim.keymap.set()`

-- [[ Toggle Terminal ]]
vim.keymap.set("n", "<leader>T", "<cmd>ToggleTerm<CR>", { desc = "Toggle terminal" })

-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.opt.hlsearch = true
vim.keymap.set("n", "<Esc>", "<cmd>nohlsearch<CR>", { desc = "Disable search highlighting" })

-- TIP: Disable arrow keys in normal mode
vim.keymap.set("n", "<left>", '<cmd>echo "Use h to move!!"<CR>')
vim.keymap.set("n", "<right>", '<cmd>echo "Use l to move!!"<CR>')
vim.keymap.set("n", "<up>", '<cmd>echo "Use k to move!!"<CR>')
vim.keymap.set("n", "<down>", '<cmd>echo "Use j to move!!"<CR>')

-- Diagnostic keymaps
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous [D]iagnostic message" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next [D]iagnostic message" })
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Show diagnostic [E]rror messages" })
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostic [Q]uickfix list" })

-- Exit terminal mode in the builtin terminal with a shortcut that is a bit easier
-- for people to discover. Otherwise, you normally need to press <C-\><C-n>, which
-- is not what someone will guess without a bit more experience.
--
-- NOTE: This won't work in all terminal emulators/tmux/etc. Try your own mapping
-- or just use <C-\><C-n> to exit terminal mode
vim.keymap.set("t", "<Esc><Esc>", "<C-\\><C-n>", { desc = "Exit terminal mode" })
-- Keybinds to make split navigation easier.
--  Use CTRL+<hjkl> to switch between windows
--
--  See `:help wincmd` for a list of all window commands
vim.keymap.set("n", "<C-h>", "<C-w><C-h>", { desc = "Move focus to the left window" })
vim.keymap.set("n", "<C-l>", "<C-w><C-l>", { desc = "Move focus to the right window" })
vim.keymap.set("n", "<C-j>", "<C-w><C-j>", { desc = "Move focus to the lower window" })
vim.keymap.set("n", "<C-k>", "<C-w><C-k>", { desc = "Move focus to the upper window" })

-- Set keymap to toggle the filetree
vim.keymap.set("n", "<leader>q", ":NvimTreeToggle<CR>") --, { noremap = true, silent = true })

-- Set keymaps to encrypt and decrypt files with sops
vim.keymap.set("n", "<leader>se", vim.cmd.SopsEncrypt, { desc = "[S]ops [E]ncrypt" })
vim.keymap.set("n", "<leader>sd", vim.cmd.SopsDecrypt, { desc = "[S]ops [E]ncrypt" })

-- Shortcut to open NeoGit tab
vim.keymap.set("n", "<A-g>", ":Neogit<CR>", { desc = "[G]it" })
vim.keymap.set("n", "<A-c>", ":Neogit commit<CR>", { desc = "Git [C]ommit" })

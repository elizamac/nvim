-- NOTE: Autoclose parenthesis and quotes
return {
	"m4xshen/autoclose.nvim",
	event = { "InsertEnter" },
	config = function()
		require("autoclose").setup({
			options = {
				pair_spaces = true,
				disabled_filetypes = { "text", "markdown" },
			},
		})
	end,
}

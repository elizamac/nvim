-- NOTE: Toggleable terminal inside nvim

local term
function GetTerm()
	term = os.getenv("SHELL")
	if term == nil then
		term = "powershell"
	end
end

return {
	"akinsho/toggleterm.nvim",
	version = "*",
	opts = {
		direction = "float",
		close_on_exit = false,
		shell = term,
		float_opts = {
			border = "curved",
		},
	},
}

-- NOTE: Catppuccin theme
return {
	"catppuccin/nvim",
	name = "catppuccin",
	priority = 1000,
	config = function()
		require("catppuccin").setup({
			style = "mocha",
			transparent_background = true,
			terminal_colors = true,
			background = {
				dark = "mocha",
			},
		})
		vim.cmd.colorscheme("catppuccin")
	end,
}

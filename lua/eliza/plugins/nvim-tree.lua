return {
	"nvim-tree/nvim-tree.lua",
	version = "*",
	lazy = false,
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	config = function()
		require("nvim-tree").setup({
			hijack_cursor = true,
			filters = {
				git_ignored = false,
				dotfiles = false,
			},
			actions = {
				open_file = {
					quit_on_open = true,
				},
			},
			renderer = {
				indent_markers = {
					enable = true,
				},
				icons = {
					web_devicons = {
						folder = {
							enable = true,
						},
					},
					glyphs = {
						folder = {
							arrow_closed = "▶",
							arrow_open = "▼",
						},
					},
				},
			},
		})
	end,
}

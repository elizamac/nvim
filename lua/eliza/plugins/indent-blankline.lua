return {
	"lukas-reineke/indent-blankline.nvim",
	event = { "BufReadPre", "BufNewFile" },
	main = "ibl",
	opts = {
		enabled = true,
		indent = {
			char = "▍",
			tab_char = "▍",
			smart_indent_cap = true,
		},
	},
	-- config = function()
	-- 	require("ibl").setup({
	-- 		enabled = true,
	-- 		indent = { char = "▍" },
	-- 	})
	-- end,
}

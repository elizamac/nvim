-- NOTE: Git integration
return {
	"NeogitOrg/neogit",
	cmd = {
		"Neogit",
		"NeogitReserState",
	},
	dependencies = {
		"nvim-lua/plenary.nvim", -- required
		"sindrets/diffview.nvim", -- optional - Diff integration

		"nvim-telescope/telescope.nvim",
	},
	config = true,
}
